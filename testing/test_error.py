from src.error import error
from src.tokens import MetaToken
from pytest import raises

def test_exit():
    with raises(SystemExit):
        error(MetaToken(0, 0, ""), "", "")