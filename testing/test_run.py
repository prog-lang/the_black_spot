from src.run import Runner, run
from src.tokens import Token
from src.token_types import TokenType
from pytest import raises

def test_empty():
    run([], {})

def test_number():
    t = Runner([Token(0, 0, "", TokenType.NUMBER)], {})
    t.run()
    assert t.stack == [0]

def test_numbers():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(1230, 0, "", TokenType.NUMBER)], {})
    t.run()
    assert t.stack == [0, 1230]

def test_add():
    t = Runner([Token(1, 0, "", TokenType.NUMBER), Token(1, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {})
    t.run()
    assert t.stack == [2]

def test_add_more_args():
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(2, 0, "", TokenType.NUMBER), Token(3, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {})
    t.run()
    assert t.stack == [3, 5]

def test_add_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.ADD)], {})
    with raises(SystemExit):
        t.run()

def test_add_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {})
    with raises(SystemExit):
        t.run()

def test_subtract():
    t = Runner([Token(2, 0, "", TokenType.NUMBER), Token(1, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {})
    t.run()
    assert t.stack == [1]
    
def test_subtract_more_args():
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(3, 0, "", TokenType.NUMBER), Token(2, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {})
    t.run()
    assert t.stack == [3, 1]

def test_subtract_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.SUBTRACT)], {})
    with raises(SystemExit):
        t.run()

def test_subtract_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {})
    with raises(SystemExit):
        t.run()

def test_multiply():
    t = Runner([Token(2, 0, "", TokenType.NUMBER), Token(3, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {})
    t.run()
    assert t.stack == [6]

def test_multiply_more_args():
    t = Runner([Token(2, 0, "", TokenType.NUMBER), Token(4, 0, "", TokenType.NUMBER), Token(5, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {})
    t.run()
    assert t.stack == [2, 20]

def test_multiply_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.MULTIPLY)], {})
    with raises(SystemExit):
        t.run()

def test_multiply_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {})
    with raises(SystemExit):
        t.run()

def test_divide():
    t = Runner([Token(6, 0, "", TokenType.NUMBER), Token(3, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {})
    t.run()
    assert t.stack == [2]
    
def test_divide_more_args():
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(4, 0, "", TokenType.NUMBER), Token(2, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {})
    t.run()
    assert t.stack == [3, 2]

def test_divide_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.DIVIDE)], {})
    with raises(SystemExit):
        t.run()

def test_divide_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {})
    with raises(SystemExit):
        t.run()

def test_print(capfd):
    t = Runner([Token(24, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT)], {})
    t.run()
    out, err = capfd.readouterr()
    assert t.stack == []
    assert out == "24"

def test_print_more_args(capfd):
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(29, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT)], {})
    t.run()
    out, err = capfd.readouterr()
    assert t.stack == [3]
    assert out == "29"

def test_print_failure():
    t = Runner([Token(0, 0, "", TokenType.PRINT)], {})
    with raises(SystemExit):
        t.run()

def test_print_ascii(capfd):
    t = Runner([Token(63, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT_ASCII)], {})
    t.run()
    out, err = capfd.readouterr()
    assert t.stack == []
    assert out == "?"

def test_print_ascii_more_args(capfd):
    t = Runner([Token(102, 0, "", TokenType.NUMBER), Token(100, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT_ASCII)], {})
    t.run()
    out, err = capfd.readouterr()
    assert t.stack == [102]
    assert out == "d"

def test_print_ascii_failure():
    t = Runner([Token(0, 0, "", TokenType.PRINT_ASCII)], {})
    with raises(SystemExit):
        t.run()

def test_parameter():
    t = Runner([Token(0, 0, "", TokenType.PARAMETER)], {})
    t.run()
    assert t.stack == []

def test_parameter_more_args():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PARAMETER)], {})
    t.run()
    assert t.stack == [0]

def test_drop():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DROP)], {})
    t.run()
    assert t.stack == []

def test_drop_more_args():
    t = Runner([Token(6, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DROP)], {})
    t.run()
    assert t.stack == [6]

def test_drop_failure():
    t = Runner([Token(0, 0, "", TokenType.DROP)], {})
    with raises(SystemExit):
        t.run()

def test_duplicate():
    t = Runner([Token(4, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DUPLICATE)], {})
    t.run()
    assert t.stack == [4, 4]

def test_duplicate_more_args():
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(7, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DUPLICATE)], {})
    t.run()
    assert t.stack == [3, 7, 7]

def test_duplicate_failure():
    t = Runner([Token(0, 0, "", TokenType.DUPLICATE)], {})
    with raises(SystemExit):
        t.run()

def test_swap():
    t = Runner([Token(3, 0, "", TokenType.NUMBER), Token(9, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {})
    t.run()
    assert t.stack == [9, 3]

def test_swap_more_args():
    t = Runner([Token(1, 0, "", TokenType.NUMBER), Token(10, 0, "", TokenType.NUMBER), Token(4, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {})
    t.run()
    assert t.stack == [1, 4, 10]

def test_swap_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.SWAP)], {})
    with raises(SystemExit):
        t.run()

def test_swap_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {})
    with raises(SystemExit):
        t.run()

def test_over():
    t = Runner([Token(8, 0, "", TokenType.NUMBER), Token(9, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {})
    t.run()
    assert t.stack == [8, 9, 8]

def test_over_more_args():
    t = Runner([Token(1, 0, "", TokenType.NUMBER), Token(2, 0, "", TokenType.NUMBER), Token(3, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {})
    t.run()
    assert t.stack == [1, 2, 3, 2]

def test_over_failure_no_args():
    t = Runner([Token(0, 0, "", TokenType.OVER)], {})
    with raises(SystemExit):
        t.run()

def test_over_failure_one_arg():
    t = Runner([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {})
    with raises(SystemExit):
        t.run()