from src import lexing
from src.tokens import MetaToken, Token
from src.token_types import TokenType
import pytest

def test_lex_file_empty():
    assert lexing.lex_file("testing/test_files/empty.txt") == []

def test_lex_file_blank():
    assert lexing.lex_file("testing/test_files/blank.txt") != []

def test_lex_lines_empty():
    assert lexing.lex_lines([], "") == []

def test_lex_lines_blank():
    assert lexing.lex_lines(["", " ", "\t", " "*10, "\t"*10], "") == []

def test_lex_lines_non_dots():
    assert lexing.lex_lines([
        "", "asdjkfn alskdj ",
        "0912he0in2odj", "aslknd",
        "skfvldj lskjdnv/,/s,s/d,fsvsed'v",
        "1234567890qwertyuiopasdfghjklzxcvbnm",
        "!@#$%^&*()QWERTYUIOPASDFGHJKLZXCVBNM",
        "`~-=_+[]\\{{}}|;:'\",/<?>",
        " \t    ",
        "\n"
    ], "") == []

def test_lex_lines_dot():
    assert lexing.lex_lines([
        "."
    ], "") == [MetaToken(1, 0, "")]

def test_lex_lines_dot_with_alphanumeric():
    assert lexing.lex_lines([
        "alsk.asdlkasdlkasd askjdnaksj "
    ], "") == [MetaToken(1, 0, "")]

def test_lex_lines_dot_multi():
    assert lexing.lex_lines([
        ".........."
    ], "") == [MetaToken(10, 0, "")]

def test_lex_lines_dot_multi_lines():
    assert lexing.lex_lines([
        "..........",
        ".....   ",
        "........."
    ], "") == [
        MetaToken(10, 0, ""),
        MetaToken(5, 1, ""),
        MetaToken(9, 2, "")
    ]

def test_lex_lines_file_name():
    assert lexing.lex_lines([
        "."
    ], "file name") == [
        MetaToken(1, 0, "file name")
    ]

def test_get_token_type_invalid_negative():
    for i in range(-100, -1):
        assert lexing.get_token_type(i) == TokenType.INVALID

def test_get_token_type_numbers():
    assert lexing.get_token_type(0) == TokenType.NUMBER
    assert lexing.get_token_type(1) == TokenType.NUMBER
    assert lexing.get_token_type(2) == TokenType.NUMBER
    assert lexing.get_token_type(3) == TokenType.NUMBER
    assert lexing.get_token_type(4) == TokenType.NUMBER
    assert lexing.get_token_type(5) == TokenType.NUMBER
    assert lexing.get_token_type(6) == TokenType.NUMBER
    assert lexing.get_token_type(7) == TokenType.NUMBER
    assert lexing.get_token_type(8) == TokenType.NUMBER
    assert lexing.get_token_type(9) == TokenType.NUMBER

def test_get_token_type_arithmetic():
    assert lexing.get_token_type(10) == TokenType.ADD
    assert lexing.get_token_type(11) == TokenType.SUBTRACT
    assert lexing.get_token_type(12) == TokenType.MULTIPLY
    assert lexing.get_token_type(13) == TokenType.DIVIDE

def test_get_token_type_io():
    assert lexing.get_token_type(14) == TokenType.PRINT
    assert lexing.get_token_type(15) == TokenType.PRINT_ASCII

def test_get_token_type_jump():
    assert lexing.get_token_type(16) == TokenType.JUMP
    assert lexing.get_token_type(17) == TokenType.JUMP_CONDITIONAL

def test_get_token_type_stack_operations():
    assert lexing.get_token_type(18) == TokenType.DROP
    assert lexing.get_token_type(19) == TokenType.DUPLICATE
    assert lexing.get_token_type(20) == TokenType.SWAP
    assert lexing.get_token_type(21) == TokenType.OVER

def test_get_token_type_conditional():
    assert lexing.get_token_type(22) == TokenType.EQUALS
    assert lexing.get_token_type(23) == TokenType.GREATER_THAN
    assert lexing.get_token_type(24) == TokenType.LESS_THAN

def test_get_token_type_logical():
    assert lexing.get_token_type(25) == TokenType.AND
    assert lexing.get_token_type(26) == TokenType.OR
    assert lexing.get_token_type(27) == TokenType.NOT

def test_binary_operations():
    assert lexing.get_token_type(28) == TokenType.BAND
    assert lexing.get_token_type(29) == TokenType.BOR
    assert lexing.get_token_type(30) == TokenType.BNOT
    assert lexing.get_token_type(31) == TokenType.BXOR
    assert lexing.get_token_type(32) == TokenType.BSHIFT_LEFT
    assert lexing.get_token_type(33) == TokenType.BSHIFT_RIGHT

def test_conversion():
    assert lexing.get_token_type(34) == TokenType.INT_TO_BOOLEAN

def test_get_token_type_invalid_positive():
    for i in range(35, 5000):
        assert lexing.get_token_type(i) == TokenType.INVALID

def test_check_meta_token_bad_dots():
    for i in range(-100, -1):
        with pytest.raises(Exception):
            lexing.check_meta_token(MetaToken(i, 0, "askj"))

def test_check_meta_token_bad_line_number():
    for i in range(-100, -1):
        with pytest.raises(Exception):
            lexing.check_meta_token(MetaToken(0, i, "askj"))

def test_check_meta_token_bad_file_name():
    for i in range(1, 100):
        with pytest.raises(Exception):
            lexing.check_meta_token(MetaToken(0, 0, " "*i))

def test_get_token_numbers():
    for i in range(0, 10):
        assert lexing.get_token(MetaToken(i, 0, "io")) \
        == Token(i, 0, "io", TokenType.NUMBER)

def test_get_token():
    assert lexing.get_token(MetaToken(10, 0, "io")) == Token(10, 0, "io", TokenType.ADD)
    assert lexing.get_token(MetaToken(11, 0, "io")) == Token(11, 0, "io", TokenType.SUBTRACT)
    assert lexing.get_token(MetaToken(12, 0, "io")) == Token(12, 0, "io", TokenType.MULTIPLY)
    assert lexing.get_token(MetaToken(13, 0, "io")) == Token(13, 0, "io", TokenType.DIVIDE)
    assert lexing.get_token(MetaToken(14, 0, "io")) == Token(14, 0, "io", TokenType.PRINT)
    assert lexing.get_token(MetaToken(15, 0, "io")) == Token(15, 0, "io", TokenType.PRINT_ASCII)
    assert lexing.get_token(MetaToken(16, 0, "io")) == Token(16, 0, "io", TokenType.JUMP)
    assert lexing.get_token(MetaToken(17, 0, "io")) == Token(17, 0, "io", TokenType.JUMP_CONDITIONAL)
    assert lexing.get_token(MetaToken(18, 0, "io")) == Token(18, 0, "io", TokenType.DROP)
    assert lexing.get_token(MetaToken(19, 0, "io")) == Token(19, 0, "io", TokenType.DUPLICATE)
    assert lexing.get_token(MetaToken(20, 0, "io")) == Token(20, 0, "io", TokenType.SWAP)
    assert lexing.get_token(MetaToken(21, 0, "io")) == Token(21, 0, "io", TokenType.OVER)
    assert lexing.get_token(MetaToken(22, 0, "io")) == Token(22, 0, "io", TokenType.EQUALS)
    assert lexing.get_token(MetaToken(23, 0, "io")) == Token(23, 0, "io", TokenType.GREATER_THAN)
    assert lexing.get_token(MetaToken(24, 0, "io")) == Token(24, 0, "io", TokenType.LESS_THAN)
    assert lexing.get_token(MetaToken(25, 0, "io")) == Token(25, 0, "io", TokenType.AND)
    assert lexing.get_token(MetaToken(26, 0, "io")) == Token(26, 0, "io", TokenType.OR)
    assert lexing.get_token(MetaToken(27, 0, "io")) == Token(27, 0, "io", TokenType.NOT)
    assert lexing.get_token(MetaToken(28, 0, "io")) == Token(28, 0, "io", TokenType.BAND)
    assert lexing.get_token(MetaToken(29, 0, "io")) == Token(29, 0, "io", TokenType.BOR)
    assert lexing.get_token(MetaToken(30, 0, "io")) == Token(30, 0, "io", TokenType.BNOT)
    assert lexing.get_token(MetaToken(31, 0, "io")) == Token(31, 0, "io", TokenType.BXOR)
    assert lexing.get_token(MetaToken(32, 0, "io")) == Token(32, 0, "io", TokenType.BSHIFT_LEFT)
    assert lexing.get_token(MetaToken(33, 0, "io")) == Token(33, 0, "io", TokenType.BSHIFT_RIGHT)
    assert lexing.get_token(MetaToken(34, 0, "io")) == Token(34, 0, "io", TokenType.INT_TO_BOOLEAN)

def test_get_invalid_token_negative():
    for i in range(-1000, -1):
        with pytest.raises(Exception):
            lexing.get_token(MetaToken(i, 0, "io"))

def test_get_invalid_token_positive():
    for i in range(35, 5000):
        with pytest.raises(SystemExit):
            lexing.get_token(MetaToken(i, 0, "io"))

def test_lex_meta_tokens_numbers():
    assert lexing.lex_meta_tokens([
        MetaToken(0, 0, "sd"),
        MetaToken(3, 1, "sd"),
        MetaToken(7, 4, "sd"),
        MetaToken(9, 8, "sd"),
    ]) == [
        Token(0, 0, "sd", TokenType.NUMBER),
        Token(3, 1, "sd", TokenType.NUMBER),
        Token(7, 4, "sd", TokenType.NUMBER),
        Token(9, 8, "sd", TokenType.NUMBER),
    ]

def test_lex_meta_tokens_jump():
    assert lexing.lex_meta_tokens([
        MetaToken(16, 0, "sd"),
        MetaToken(12, 1, "sd"),
        MetaToken(17, 3, "sd"),
        MetaToken(139, 4, "sd"),
    ]) == [
        Token(16, 0, "sd", TokenType.JUMP),
        Token(12, 1, "sd", TokenType.PARAMETER),
        Token(17, 3, "sd", TokenType.JUMP_CONDITIONAL),
        Token(139, 4, "sd", TokenType.PARAMETER),
    ]

def test_lex_meta_tokens_jump_fail():
    with pytest.raises(SystemExit):
        lexing.lex_meta_tokens([MetaToken(16, 0, "sd")])

def test_lex_empty():
    assert lexing.lex("testing/test_files/empty.txt") == []

def test_lex_blank():
    assert lexing.lex("testing/test_files/blank.txt") == []

def test_lex_non_dots():
    assert lexing.lex("testing/test_files/non_dots.txt") == []

def tets_lex_dots_fail():
    with pytest.raises(SystemExit):
        lexing.lex("testing/test_files/non_dots.txt")