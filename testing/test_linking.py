from src import linking
from src.tokens import Token
from src.token_types import TokenType

def test_link_empty():
    assert linking.tokens_to_line_number_indexing([]) == {}

def test_link_single():
    assert linking.tokens_to_line_number_indexing([
        Token(0, 0, "", TokenType.ADD)
    ]) == {
        0 : 0
    }

def test_link_multi():
    assert linking.tokens_to_line_number_indexing([
        Token(0, 0, "", TokenType.ADD),
        Token(0, 3, "", TokenType.ADD),
        Token(0, 6, "", TokenType.ADD),
        Token(0, 2, "", TokenType.ADD),
    ]) == {
        0 : 0,
        3 : 1,
        6 : 2,
        2 : 3,
    }