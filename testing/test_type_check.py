from src.type_check import TypeCheck, DataTypes, type_check
from src.tokens import Token
from src.token_types import TokenType
from pytest import raises

def test_empty():
    type_check([], {})

def test_number():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_numbers():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_add():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_add_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_add_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.ADD)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_add_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.ADD)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_subtract():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]
    
def test_subtract_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_subtract_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.SUBTRACT)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_subtract_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SUBTRACT)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_multiply():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_multiply_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_multiply_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.MULTIPLY)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_multiply_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.MULTIPLY)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_divide():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]
    
def test_divide_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_divide_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.DIVIDE)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_divide_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DIVIDE)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_print():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT)], {}, debug=True)
    t.type_check()
    assert t.stack == []

def test_print_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_print_failure():
    t = TypeCheck([Token(0, 0, "", TokenType.PRINT)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_print_ascii():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT_ASCII)], {}, debug=True)
    t.type_check()
    assert t.stack == []

def test_print_ascii_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PRINT_ASCII)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_print_ascii_failure():
    t = TypeCheck([Token(0, 0, "", TokenType.PRINT_ASCII)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_parameter():
    t = TypeCheck([Token(0, 0, "", TokenType.PARAMETER)], {}, debug=True)
    t.type_check()
    assert t.stack == []

def test_parameter_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.PARAMETER)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_drop():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DROP)], {}, debug=True)
    t.type_check()
    assert t.stack == []

def test_drop_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DROP)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_drop_failure():
    t = TypeCheck([Token(0, 0, "", TokenType.DROP)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_duplicate():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DUPLICATE)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_duplicate_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.DUPLICATE)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT, DataTypes.INT]

def test_duplicate_failure():
    t = TypeCheck([Token(0, 0, "", TokenType.DUPLICATE)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_swap():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT]

def test_swap_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT, DataTypes.INT]

def test_swap_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.SWAP)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_swap_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.SWAP)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_over():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT, DataTypes.INT]

def test_over_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.INT, DataTypes.INT, DataTypes.INT]

def test_over_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.OVER)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_over_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OVER)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 0}, debug=True)
    t.type_check()
    assert t.stack == []

def test_jump_more_arguments():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.JUMP), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 0}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_jump_failure_no_argument():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP), ], { 0: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_failure_bad_argument():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP), Token(0, 0, "", TokenType.NUMBER), ], { 0: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_failure_bad_address():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP), Token(0, 0, "", TokenType.PARAMETER)], { 1: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_failure_bad_address_index():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 2}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_conditional():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.EQUALS), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 0}, debug=True)
    t.type_check()
    assert t.stack == []

def test_jump_conditional_more_arguments():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.EQUALS), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 0}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT]

def test_jump_conditional_failure_no_condition():
    t = TypeCheck([Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.PARAMETER)], { 0: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_conditional_failure_no_argument():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), ], { 0: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_conditional_failure_bad_argument():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.NUMBER), ], { 0: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_conditional_failure_bad_address():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.PARAMETER)], { 1: 0}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_jump_conditional_failure_bad_address_index():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.JUMP_CONDITIONAL), Token(0, 0, "", TokenType.PARAMETER), ], { 0: 3}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_equals():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.EQUALS)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.BOOLEAN]

def test_equals_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.EQUALS)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_equals_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.EQUALS)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_equals_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.EQUALS)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.BOOLEAN]

def test_greater_than():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.GREATER_THAN)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.BOOLEAN]

def test_greater_than_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.GREATER_THAN)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_greater_than_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.GREATER_THAN)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_greater_than_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.GREATER_THAN)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.BOOLEAN]

def test_less_than():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.LESS_THAN)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.BOOLEAN]

def test_less_than_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.LESS_THAN)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_less_than_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.LESS_THAN)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_less_than_more_args():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.LESS_THAN)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.BOOLEAN]

def test_and():
    t = TypeCheck([
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.AND)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.BOOLEAN]

def test_and_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.AND)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_and_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.INT_TO_BOOLEAN), Token(0, 0, "", TokenType.AND)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_and_failure_wrong_type():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.AND)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_and_more_args():
    t = TypeCheck([
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.AND)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.BOOLEAN]

def test_or():
    t = TypeCheck([
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.OR)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.BOOLEAN]

def test_or_failure_no_args():
    t = TypeCheck([Token(0, 0, "", TokenType.OR)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_or_failure_one_arg():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.INT_TO_BOOLEAN), Token(0, 0, "", TokenType.OR)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_or_failure_wrong_type():
    t = TypeCheck([Token(0, 0, "", TokenType.NUMBER), Token(0, 0, "", TokenType.OR)], {}, debug=True)
    with raises(SystemExit):
        t.type_check()

def test_or_more_args():
    t = TypeCheck([
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.NUMBER), 
        Token(0, 0, "", TokenType.INT_TO_BOOLEAN), 
        Token(0, 0, "", TokenType.OR)], {}, debug=True)
    t.type_check()
    assert t.stack == [DataTypes.INT, DataTypes.BOOLEAN]