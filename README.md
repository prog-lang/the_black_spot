# The Black Spot

A [statically typed](https://en.wikipedia.org/wiki/Type_system) [stack based](https://en.wikipedia.org/wiki/Stack-oriented_programming) [computer](https://en.wikipedia.org/wiki/Computer) [programming language](https://en.wikipedia.org/wiki/Programming_language) that is all about those dots.


## How to use

The Black Spot counts the number of dots on each line, and does something different based on the amount.

If there are `0-9` dots, then it will push that amount of dots to the stack.

Type checking is done similarly to [WASM type checking](https://binji.github.io/posts/webassembly-type-checking/)


| number of dots | instruction | stack before | stack after |
| ----------- | ----------- | ----------- | ----------- |
| 0-9 | PUSH_INT | ... | 0-9, ... |
| 10 | ADD | a, b, ... | a + b, ... |
| 11 | SUB | a, b, ... | a - b, ... |
| 12 | MUL | a, b, ... | a * b, ... |
| 13 | DIVIDE | a, b, ... | int(a / b), ... |
| 14 | PRINT | a, ... | ... |
| 15 | PRINT_ASCII | a, ... | ... |
| 16 | JUMP | ... | ... |
| 17 | JUMP_CONDITIONAL | bool, ... | ... |
| 18 | DROP | a, ... | ... |
| 19 | DUP | a, ... | a, a, ... |
| 20 | SWAP | a, b, ... | b, a, ... |
| 21 | OVER | a, b, c, ... | c, a, b, ... |
| 22 | EQUALS | a, b, ... | a == b, ... |
| 23 | GT | a, b, ... | a > b, ... |
| 24 | LT | a, b, ... | a < b, ... |
| 25 | AND | a, b, ... | a && b, ... |
| 26 | OR | a, b, ... | a \|\| b, ... |
| 27 | NOT | a, ... | !a, ... |
| 28 | BAND | a, b, ... | a & b, ... |
| 29 | BOR | a, b, ... | a \| b, ... |
| 30 | BNOT | a, ... | ~a, ... |
| 31 | BXOR | a, b, ... | a ^ b, ... |
| 32 | BSHIFT_LEFT | a, b, ... | a << b, ... |
| 33 | BSHIFT_RIGHT | a, b, ... | a >> b, ... |
| 34 | INT_TO_BOOL | a, ... | bool(a), ... |



