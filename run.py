from argparse import ArgumentParser

from src.main import main


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()
    main(args.filename)