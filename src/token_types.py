from enum import Enum

class TokenType(Enum):
    NUMBER = 'Number'
    
    ADD = "Add"
    SUBTRACT = "Subtract"
    MULTIPLY = "Multiply"
    DIVIDE = "Divide"
    
    PRINT = "Print"
    PRINT_ASCII = "Print Ascii"
    
    JUMP = "Jump"
    JUMP_CONDITIONAL = "Jump Conditional"
    
    EQUALS = "Equals"
    GREATER_THAN = "Greater Than"
    LESS_THAN = "Less Than"
    
    AND = "And"
    OR = "Or"
    NOT = "Not"
    
    BAND = "BAnd"
    BOR = "BOr"
    BNOT = "BNot"
    BXOR = "BXor"
    BSHIFT_LEFT = "BShift Left"
    BSHIFT_RIGHT = "BShift Right"
    
    INT_TO_BOOLEAN = "Int to Boolean"
    
    DROP = "Drop"
    DUPLICATE = "Duplicate"
    SWAP = "Swap"
    OVER = "Over"
    
    PARAMETER = "Parameter"
    
    INVALID = "Invalid"