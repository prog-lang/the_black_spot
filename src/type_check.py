from typing import List, Mapping
from src.tokens import Token
from src.error import error
from src.token_types import TokenType
from enum import Enum

class DataTypes(Enum):
    INT = 'int'
    BOOLEAN = 'boolean'

class TypeCheck:
    def __init__(self, tokens: List[Token], line_number_index: Mapping[int, int], debug: bool = False):
        self.index: int = 0
        self.stack: List[DataTypes] = []
        self.tokens: List[Token] = tokens
        self.line_number_index: Mapping[int, int] = line_number_index
        self.debug: bool = debug
        
    def type_check(self) -> None:
        while self.index < len(self.tokens):
            token: Token = self.tokens[self.index]
            if token.token_type == TokenType.NUMBER:
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.ADD:
                if len(self.stack) < 2:
                    error(token, "Unable to add", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT and a != b:
                    error(token, "Unable to add", f"Unable to add types {a} and {b}")
                self.stack.append(a)
                self.index += 1
                continue
            if token.token_type == TokenType.SUBTRACT:
                if len(self.stack) < 2:
                    error(token, "Unable to subtract", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT and a != b:
                    error(token, "Unable to subtract", f"Unable to subtract types {a} and {b}")
                self.stack.append(a)
                self.index += 1
                continue
            if token.token_type == TokenType.MULTIPLY:
                if len(self.stack) < 2:
                    error(token, "Unable to multiply", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT and a != b:
                    error(token, "Unable to multiply", f"Unable to multiply types {a} and {b}")
                self.stack.append(a)
                self.index += 1
                continue
            if token.token_type == TokenType.DIVIDE:
                if len(self.stack) < 2:
                    error(token, "Unable to divide", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT and a != b:
                    error(token, "Unable to divide", f"Unable to divide types {a} and {b}")
                self.stack.append(a)
                self.index += 1
                continue
                
            if token.token_type == TokenType.PRINT:
                if len(self.stack) < 1:
                    error(token, "Unable to print", "Not enought items in stack.")
                a = self.stack.pop()
                if a != DataTypes.INT:
                    error(token, "Unable to print", f"Unable to print type {a}")
                self.index += 1
                continue
            if token.token_type == TokenType.PRINT_ASCII:
                if len(self.stack) < 1:
                    error(token, "Unable to print ascii", "Not enought items in stack.")
                a = self.stack.pop()
                if a != DataTypes.INT:
                    error(token, "Unable to print ascii", f"Unable to print type {a}")
                self.index += 1
                continue
            
            if token.token_type == TokenType.JUMP:
                self.index += 1
                if self.index >= len(self.tokens):
                    error(token, "Unexpected file end", "Jump expects a line number, got file end instead.")
                token = self.tokens[self.index]
                if token.token_type != TokenType.PARAMETER:
                    error(token, "Illegal parameter token", f"Got {token.token_type} instead")
                if token.num_dots not in self.line_number_index:
                    error(token, "Illegal jump address", f"Jump address must be a line number pointing to a valid instruction. Found: {token.num_dots} which is either too large for the file or is pointing to a line with no dots.")
                new_index = self.line_number_index[token.num_dots]
                if not (0 <= new_index < len(self.tokens)):
                    error(token, "Illegal jump address", f"Jump address does not point to a valid token")
                continue
            
            if token.token_type == TokenType.JUMP_CONDITIONAL:
                if len(self.stack) < 1:
                    error(token, "Unable to jump conditionally", "Not enough items on the stack for condition")
                a = self.stack.pop()
                if a != DataTypes.BOOLEAN:
                    error(token, "Unable to jump conditionally", f"Expected type boolean. Found type {a}")
                self.index += 1
                if self.index >= len(self.tokens):
                    error(token, "Unexpected file end", "Conditional jump expects a line number, got file end instead.")
                token = self.tokens[self.index]
                if token.token_type != TokenType.PARAMETER:
                    error(token, "Illegal parameter token", f"Got {token.token_type} instead")
                if token.num_dots not in self.line_number_index:
                    error(token, "Illegal conditional jump address", f"Conditional jump address must be a line number pointing to a valid instruction. Found: {token.num_dots} which is either too large for the file or is pointing to a line with no dots.")
                new_index = self.line_number_index[token.num_dots]
                if not (0 <= new_index < len(self.tokens)):
                    error(token, "Illegal conditional jump address", f"Conditional jump address does not point to a valid token")
                continue
            
            if token.token_type == TokenType.PARAMETER:
                self.index += 1
                continue
            
            if token.token_type == TokenType.DROP:
                if len(self.stack) < 1:
                    error(token, "Unable to drop element", "Not enough items on stack")
                self.stack.pop()
                self.index += 1
                continue
            
            if token.token_type == TokenType.DUPLICATE:
                if len(self.stack) < 1:
                    error(token, "Unable to duplicate element", "Not enough items on stack")
                a = self.stack.pop()
                self.stack.append(a)
                self.stack.append(a)
                self.index += 1
                continue
            
            if token.token_type == TokenType.SWAP:
                if len(self.stack) < 2:
                    error(token, "Unable to swap elements", "Not enough items on stack")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b)
                self.stack.append(a)
                self.index += 1
                continue
                
            if token.token_type == TokenType.OVER:
                if len(self.stack) < 2:
                    error(token, "Unable to over elements", "Not enough items on stack")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(a)
                self.stack.append(b)
                self.stack.append(a)
                self.index += 1
                continue
            
            if token.token_type == TokenType.EQUALS:
                if len(self.stack) < 2:
                    error(token, "Unable to do equals", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != b:
                    error(token, "Unable to do equals", f"Types to not match. Found {a} and {b}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
            
            if token.token_type == TokenType.GREATER_THAN:
                if len(self.stack) < 2:
                    error(token, "Unable to do greater than", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != b:
                    error(token, "Unable to do greater than", f"Types to not match. Found {a} and {b}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
            
            if token.token_type == TokenType.LESS_THAN:
                if len(self.stack) < 2:
                    error(token, "Unable to do less than", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != b:
                    error(token, "Unable to do less than", f"Types to not match. Found {a} and {b}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
                
            if token.token_type == TokenType.AND:
                if len(self.stack) < 2:
                    error(token, "Unable to do logical and", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.BOOLEAN or a != b:
                    error(token, "Unable to do logical and", f"Expected boolean AND boolean. Found {a} and {b}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
            
            if token.token_type == TokenType.OR:
                if len(self.stack) < 2:
                    error(token, "Unable to do logical or", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.BOOLEAN or a != b:
                    error(token, "Unable to do logical or", f"Expected boolean OR boolean. Found {a} and {b}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
                
            if token.token_type == TokenType.NOT:
                if len(self.stack) < 1:
                    error(token, "Unable to do logical not", "Not enough items on stack.")
                a = self.stack.pop()
                if a != DataTypes.BOOLEAN:
                    error(token, "Unable to do logical not", f"Expected boolean. Found {a}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BAND:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary and", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT or a != b:
                    error(token, "Unable to do binary and", f"Expected ints. Found {a} and {b}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BOR:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary or", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT or a != b:
                    error(token, "Unable to do binary or", f"Expected ints. Found {a} and {b}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BNOT:
                if len(self.stack) < 1:
                    error(token, "Unable to do binary not", "Not enough items on stack.")
                a = self.stack.pop()
                if a != DataTypes.INT:
                    error(token, "Unable to do binary not", f"Expected integer. Found {a}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BXOR:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary xor", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT or a != b:
                    error(token, "Unable to do binary xor", f"Expected ints. Found {a} and {b}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BSHIFT_LEFT:
                if len(self.stack) < 2:
                    error(token, "Unable to do bit shift left", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT or a != b:
                    error(token, "Unable to do bit shift left", f"Expected ints. Found {a} and {b}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.BSHIFT_RIGHT:
                if len(self.stack) < 2:
                    error(token, "Unable to do bit shift right", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                if a != DataTypes.INT or a != b:
                    error(token, "Unable to do bit shift right", f"Expected ints. Found {a} and {b}")
                self.stack.append(DataTypes.INT)
                self.index += 1
                continue
            
            if token.token_type == TokenType.INT_TO_BOOLEAN:
                if len(self.stack) < 1:
                    error(token, "Unable to do int to boolean conversion", "Not enough items on stack.")
                a = self.stack.pop()
                if a != DataTypes.INT:
                    error(token, "Unable to do binary or", f"Expected integer. Found {a}")
                self.stack.append(DataTypes.BOOLEAN)
                self.index += 1
                continue
            
            error(token, "Invalid token", f"{token.num_dots} dots is not a valid operation")
        
        if not self.debug and len(self.stack):
            error(token, "Stack error", f"{len(self.stack)} elements are still in the stack upon completion")
            

def type_check(tokens: List[Token], line_number_index: Mapping[int, int]) -> None:
    TypeCheck(tokens, line_number_index).type_check()