from typing import List, Mapping
from src.lexing import lex
from src.tokens import Token
from src.linking import tokens_to_line_number_indexing
from src.type_check import type_check
from src.run import run

def main(file_name: str) -> None:
    tokens: List[Token] = lex(file_name)
    indexing: Mapping[int, int] = tokens_to_line_number_indexing(tokens)
    type_check(tokens, indexing)
    run(tokens, indexing)
