from dataclasses import dataclass
from src.token_types import TokenType

@dataclass
class MetaToken:
    num_dots: int
    line_number: int
    file_name: str

@dataclass
class Token(MetaToken):
    token_type: TokenType