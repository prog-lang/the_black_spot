from src.tokens import MetaToken
from sys import exit

def error(token: MetaToken, error_type: str, reason: str) -> None:
    print(f"{token.file_name}:{token.line_number+1} {error_type}: {reason}")
    exit(1)