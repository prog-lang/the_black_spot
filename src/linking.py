from typing import List, Mapping
from src.tokens import Token

def tokens_to_line_number_indexing(tokens: List[Token]) -> Mapping[int, int]:
    out = {}
    for i, token in enumerate(tokens):
        out[token.line_number] = i
    return out