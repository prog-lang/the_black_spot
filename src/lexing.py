from typing import List, Mapping
from re import search
from src.tokens import MetaToken, Token
from src.token_types import TokenType
from src.error import error

def lex_file(file_name: str) -> List[str]:
    with open(file_name, 'r') as f:
        return f.readlines()
    
def lex_lines(lines: List[str], file_name: str) -> List[MetaToken]:
    out: List[MetaToken] = []
    for i, line in enumerate(lines):
        if "." not in line:
            continue
        regex = search("[.]+", line.strip())
        if regex is None:
            raise Exception("Could not find any dots")
        dots = regex.group(0)
        out.append(MetaToken(len(dots), i, file_name))
    return out

def get_token_type(num_dots: int) -> TokenType:
    if 0 <= num_dots <= 9:
        return TokenType.NUMBER
    lookup: Mapping[int, TokenType] = {
        10: TokenType.ADD,
        11: TokenType.SUBTRACT,
        12: TokenType.MULTIPLY,
        13: TokenType.DIVIDE,
        14: TokenType.PRINT,
        15: TokenType.PRINT_ASCII,
        16: TokenType.JUMP,
        17: TokenType.JUMP_CONDITIONAL,
        18: TokenType.DROP,
        19: TokenType.DUPLICATE,
        20: TokenType.SWAP,
        21: TokenType.OVER,
        22: TokenType.EQUALS,
        23: TokenType.GREATER_THAN,
        24: TokenType.LESS_THAN,
        25: TokenType.AND,
        26: TokenType.OR,
        27: TokenType.NOT,
        28: TokenType.BAND,
        29: TokenType.BOR,
        30: TokenType.BNOT,
        31: TokenType.BXOR,
        32: TokenType.BSHIFT_LEFT,
        33: TokenType.BSHIFT_RIGHT,
        34: TokenType.INT_TO_BOOLEAN,
    }
    return lookup.get(num_dots, TokenType.INVALID)

def check_meta_token(token: MetaToken) -> None:
    if token.num_dots < 0:
        raise Exception(f"Invalid number of dots {token.num_dots}")
    if token.line_number < 0:
        raise Exception(f"Invalid token line number {token.line_number}")
    if len(token.file_name.strip()) == 0:
        raise Exception(f"Invalid token file name")

def get_token(meta_token: MetaToken) -> Token:
    check_meta_token(meta_token)
    
    token_type: TokenType = get_token_type(meta_token.num_dots)
    if token_type == TokenType.INVALID:
        error(meta_token, "Invalid token", f"Invalid number of dots {meta_token.num_dots}")
    
    return Token(meta_token.num_dots, meta_token.line_number, meta_token.file_name, token_type)

def lex_meta_tokens(tokens: List[MetaToken]) -> List[Token]:
    out: List[Token] = []
    index = 0
    while index < len(tokens):
        meta_token: MetaToken = tokens[index]
        token: Token = get_token(meta_token)
        out.append(token)
        index += 1
        
        # Is the current instruction needing a paramater?
        assert get_token_type(16) == TokenType.JUMP
        assert get_token_type(17) == TokenType.JUMP_CONDITIONAL
        if meta_token.num_dots in [16, 17]:
            if index >= len(tokens):
                error(token, "Expected parameter", "Found end of file")
            meta_token = tokens[index]
            out.append(Token(meta_token.num_dots, meta_token.line_number, meta_token.file_name, TokenType.PARAMETER))
            index += 1
    return out

def lex(file_name: str) -> List[Token]:
    lines: List[str] = lex_file(file_name)
    meta_tokens: List[MetaToken] = lex_lines(lines, file_name)
    return lex_meta_tokens(meta_tokens)
