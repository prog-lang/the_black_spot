from typing import List, Mapping
from src.tokens import Token
from src.token_types import TokenType
from src.error import error

class Runner:
    def __init__(self, tokens: List[Token], line_number_index: Mapping[int, int]) -> None:
        self.tokens: List[Token] = tokens
        self.line_number_index: Mapping[int, int] = line_number_index
        self.index: int = 0
        self.stack: List[int] = []
    
    def run(self) -> None:
        while self.index < len(self.tokens):
            token: Token = self.tokens[self.index]
            
            if token.token_type == TokenType.NUMBER:
                self.stack.append(token.num_dots)
                self.index += 1
                continue
            
            if token.token_type == TokenType.ADD:
                if len(self.stack) < 2:
                    error(token, "Unable to add", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b + a)
                self.index += 1
                continue
            if token.token_type == TokenType.SUBTRACT:
                if len(self.stack) < 2:
                    error(token, "Unable to subtract", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b - a)
                self.index += 1
                continue
            if token.token_type == TokenType.MULTIPLY:
                if len(self.stack) < 2:
                    error(token, "Unable to multiply", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b * a)
                self.index += 1
                continue
            if token.token_type == TokenType.DIVIDE:
                if len(self.stack) < 2:
                    error(token, "Unable to divide", "Not enough items in stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b // a)
                self.index += 1
                continue
            
            if token.token_type == TokenType.PRINT:
                if len(self.stack) < 1:
                    error(token, "Unable to print", "Not enough items in stack.")
                print(self.stack.pop(), end="")
                self.index += 1
                continue
            if token.token_type == TokenType.PRINT_ASCII:
                if len(self.stack) < 1:
                    error(token, "Unable to print ascii", "Not enough items in stack.")
                print(chr(self.stack.pop()), end="")
                self.index += 1
                continue
            
            if token.token_type == TokenType.JUMP:
                self.index += 1
                if self.index >= len(self.tokens):
                    error(token, "Unexpected file end", "Jump expects a line number, got file end instead.")
                token = self.tokens[self.index]
                if token.token_type != TokenType.PARAMETER:
                    error(token, "Illegal type", "Expects parameter")
                if token.num_dots not in self.line_number_index:
                    error(token, "Illegal jump address", f"Jump address must be a line number pointing to a valid instruction. Found: {token.num_dots} which is either too large for the file or is pointing to a line with no dots.")
                new_index = self.line_number_index[token.num_dots]
                if not (0 <= new_index < len(self.tokens)):
                    error(token, "Illegal jump address", f"Jump address does not point to a valid token")
                self.index = new_index - 1
                continue
            
            if token.token_type == TokenType.JUMP_CONDITIONAL:
                if len(self.stack) < 1:
                    error(token, "Unable to jump conditionally", "Not enough items on the stack for condition")
                condition = self.stack.pop()
                self.index += 1
                if condition != 1:
                    continue
                if self.index >= len(self.tokens):
                    error(token, "Unexpected file end", "Conditional jump expects a line number, got file end instead.")
                token = self.tokens[self.index]
                if token.token_type != TokenType.PARAMETER:
                    error(token, "Illegal type", "Expects parameter")
                if token.num_dots not in self.line_number_index:
                    error(token, "Illegal conditional jump address", f"Conditional jump address must be a line number pointing to a valid instruction. Found: {token.num_dots} which is either too large for the file or is pointing to a line with no dots.")
                new_index = self.line_number_index[token.num_dots]
                if not (0 <= new_index < len(self.tokens)):
                    error(token, "Illegal conditional jump address", f"Conditional jump address does not point to a valid token")
                self.index = new_index - 1
                continue
            
            if token.token_type == TokenType.PARAMETER:
                self.index += 1
                continue
            
            if token.token_type == TokenType.DROP:
                if len(self.stack) < 1:
                    error(token, "Unable to drop element", "Not enough items on stack")
                self.stack.pop()
                self.index += 1
                continue
            
            if token.token_type == TokenType.DUPLICATE:
                if len(self.stack) < 1:
                    error(token, "Unable to duplicate element", "Not enough items on stack")
                a = self.stack.pop()
                self.stack.append(a)
                self.stack.append(a)
                self.index += 1
                continue
            
            if token.token_type == TokenType.SWAP:
                if len(self.stack) < 2:
                    error(token, "Unable to swap elements", "Not enough items on stack")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(a)
                self.stack.append(b)
                self.index += 1
                continue
                
            if token.token_type == TokenType.OVER:
                if len(self.stack) < 2:
                    error(token, "Unable to over elements", "Not enough items on stack")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(b)
                self.stack.append(a)
                self.stack.append(b)
                self.index += 1
                continue
            
            if token.token_type == TokenType.EQUALS:
                if len(self.stack) < 2:
                    error(token, "Unable to do equals", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(a == b))
                self.index += 1
                continue
            
            if token.token_type == TokenType.GREATER_THAN:
                if len(self.stack) < 2:
                    error(token, "Unable to do greater than", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b > a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.LESS_THAN:
                if len(self.stack) < 2:
                    error(token, "Unable to do less than", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b < a))
                self.index += 1
                continue
                
            if token.token_type == TokenType.AND:
                if len(self.stack) < 2:
                    error(token, "Unable to do logical and", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(a and b))
                self.index += 1
                continue
            
            if token.token_type == TokenType.OR:
                if len(self.stack) < 2:
                    error(token, "Unable to do logical or", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(a or b))
                self.index += 1
                continue
                
            if token.token_type == TokenType.NOT:
                if len(self.stack) < 1:
                    error(token, "Unable to do logical not", "Not enough items on stack.")
                a = self.stack.pop()
                self.stack.append(int(not a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BAND:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary and", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b & a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BOR:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary or", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b | a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BNOT:
                if len(self.stack) < 1:
                    error(token, "Unable to do binary not", "Not enough items on stack.")
                a = self.stack.pop()
                self.stack.append(int(~a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BXOR:
                if len(self.stack) < 2:
                    error(token, "Unable to do binary xor", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(a ^ b))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BSHIFT_LEFT:
                if len(self.stack) < 2:
                    error(token, "Unable to do bit shift left", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b << a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.BSHIFT_RIGHT:
                if len(self.stack) < 2:
                    error(token, "Unable to do bit shift right", "Not enough items on stack.")
                a = self.stack.pop()
                b = self.stack.pop()
                self.stack.append(int(b >> a))
                self.index += 1
                continue
            
            if token.token_type == TokenType.INT_TO_BOOLEAN:
                if len(self.stack) < 1:
                    error(token, "Unable to do int to boolean conversion", "Not enough items on stack.")
                a = self.stack.pop()
                self.stack.append(int(bool(a)))
                self.index += 1
                continue
            
            error(token, "Invalid token", f"{token.num_dots} dots is not a valid operator")

def run(tokens: List[Token], line_number_index: Mapping[int, int]) -> None:
    Runner(tokens, line_number_index).run()